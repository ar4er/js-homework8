// Опишіть своїми словами що таке Document Object Model (DOM)
// Структура html документа в виде обьектов JS со своими свойствами (методами).

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerText устанавливает текстовый контент. Если впишем html код то он отобразится как текст.
// innerHTML устанавливает и контент, его форматирование (<b></b>, <i></i>), стили и тд.

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// По классу document.getElementsByClassName("class") ,  по id document.getElementById("id") ,
// по тэгу document.getElementsByTagName("tagName").
// Универсальный способ document.querySelector(".class" / "#id" / "tagName").
// Каждый для своих целей. Проще всего запомнить один (querySelector).

const paragraphs = document.getElementsByTagName("p");
Array.from(paragraphs).forEach((el) => {
  el.style.background = "#ff0000";
});

// function paragraphsChangeColor(elem) {
//   elem.style.backgroundColor = "#ff0000";
// }

// Array.from(paragraphs).forEach(paragraphsChangeColor);

const optionsList = document.getElementById("optionsList");

console.log("optionsList :>> ", optionsList);
console.log("==========================");
console.log("optionsList.parentElement :>> ", optionsList.parentElement);
console.log("==========================");
console.log("optionsList.children :>> ", optionsList.children);
console.log("==========================");

const testParagraph = document.getElementById("testParagraph");
testParagraph.innerHTML = "This is a paragraph";
console.log("testParagraph :>> ", testParagraph);
console.log("==========================");

const mainHeader = document.querySelector(".main-header");
const mainHeaderList = mainHeader.getElementsByTagName("li");
console.log("mainHeaderList :>> ", mainHeaderList);
console.log("==========================");

Array.from(mainHeaderList).forEach((el) => {
  el.className = "nav-item";
});

console.log("mainHeaderList :>> ", mainHeaderList);
console.log("==========================");

const sectionTitle = document.getElementsByClassName("section-title");

Array.from(sectionTitle).forEach((el, index) => {
  console.log(`sectionTitle parent element ${index + 1}:>> `, el.parentElement);
});

console.log("sectionTitle with class:>> ", sectionTitle);
console.log("==========================");

Array.from(sectionTitle).forEach((el) => {
  el.classList.toggle("section-title");
});

console.log("sectionTitle without class:>> ", sectionTitle);
console.log("==========================");
